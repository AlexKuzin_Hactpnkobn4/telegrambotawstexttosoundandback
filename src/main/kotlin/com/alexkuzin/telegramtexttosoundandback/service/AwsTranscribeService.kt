package com.alexkuzin.telegramtexttosoundandback.service

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.transcribe.AmazonTranscribe
import com.amazonaws.services.transcribe.model.DeleteTranscriptionJobRequest
import com.amazonaws.services.transcribe.model.GetTranscriptionJobRequest
import com.amazonaws.services.transcribe.model.LanguageCode
import com.amazonaws.services.transcribe.model.Media
import com.amazonaws.services.transcribe.model.StartTranscriptionJobRequest
import com.amazonaws.services.transcribe.model.TranscriptionJobStatus.*
import com.alexkuzin.telegramtexttosoundandback.data.dto.AwsTranscribeJobResponseDto
import com.alexkuzin.telegramtexttosoundandback.utils.config.AwsConfig
import com.google.gson.Gson
import org.springframework.stereotype.Service
import java.io.InputStream
import java.lang.NullPointerException
import java.lang.RuntimeException
import java.util.*
import java.util.concurrent.TimeoutException


@Service
class AwsTranscribeService(
        val awsConfig: AwsConfig,
        val s3client: AmazonS3,
        val transcribeClient: AmazonTranscribe,
        val gson: Gson
) {

    fun transcribe(inputStream: InputStream): String {
        val key = UUID.randomUUID().toString()

        putVoiceFileToS3(key, inputStream)

        startTranscribeJob(key)
        val transcription = requestForResult(key)

        removeTemporaryFiles(key)

        return transcription
    }

    private fun putVoiceFileToS3(key: String, inputStream: InputStream) {
        with(awsConfig.transcribe!!.s3) {
            s3client.putObject(
                    bucketName,
                    "$inputKeyPrefix/$key",
                    inputStream,
                    ObjectMetadata()
            )
        }
    }

    private fun getResultFromS3(key: String): AwsTranscribeJobResponseDto {
        with(awsConfig.transcribe!!.s3) {
            val data = s3client
                    .getObject(bucketName, "$outputKeyPrefix/$key")
                    .objectContent.readAllBytes()
            val json = String(data)

            return gson.fromJson(json, AwsTranscribeJobResponseDto::class.java)
        }
    }

    private fun startTranscribeJob(key: String) {
        with(awsConfig.transcribe!!.s3) {
            val request = StartTranscriptionJobRequest()
                    .withMedia(Media().withMediaFileUri("s3://$bucketName/$inputKeyPrefix/$key"))
                    .withTranscriptionJobName(key)
                    .withOutputBucketName(bucketName)
                    .withOutputKey("$outputKeyPrefix/$key")
                    .withLanguageCode(LanguageCode.EnUS)

            transcribeClient.startTranscriptionJob(request)
        }
    }

    private fun requestForResult(key: String): String {
        var millisSpent = 0L
        var result: AwsTranscribeJobResponseDto? = null

        while (millisSpent < awsConfig.transcribe!!.jobTimeoutMillis) {
            val response = transcribeClient.getTranscriptionJob(
                    GetTranscriptionJobRequest().withTranscriptionJobName(key)
            )

            val jobStatus = response.transcriptionJob.transcriptionJobStatus

            if (jobStatus == COMPLETED.toString()) {
                result = getResultFromS3(key)
                break
            } else if (jobStatus == IN_PROGRESS.toString()) {
                val delay = awsConfig.transcribe!!.jobStatusRequestingDelayMillis
                Thread.sleep(delay)
                millisSpent += delay
                if (millisSpent > awsConfig.transcribe!!.jobTimeoutMillis) {
                    throw TimeoutException()
                }
            } else if (jobStatus == FAILED.toString()) {
                throw RuntimeException("Aws transcription job has returned ${FAILED} status.")
            }
        }

        val transcription = result?.results?.transcripts?.first()?.transcript

        return transcription ?: throw NullPointerException()
    }

    fun removeTemporaryFiles(key: String) {
        with(awsConfig.transcribe!!.s3) {
            s3client.deleteObject(bucketName, "$inputKeyPrefix/$key")
            s3client.deleteObject(bucketName, "$outputKeyPrefix/$key")
        }

        transcribeClient.deleteTranscriptionJob(
                DeleteTranscriptionJobRequest()
                        .withTranscriptionJobName(key)
        )
    }

}