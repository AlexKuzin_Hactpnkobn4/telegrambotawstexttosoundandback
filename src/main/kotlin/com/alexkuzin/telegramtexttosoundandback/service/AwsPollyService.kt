package com.alexkuzin.telegramtexttosoundandback.service

import com.amazonaws.services.polly.AmazonPolly
import com.amazonaws.services.polly.model.DescribeVoicesRequest
import com.amazonaws.services.polly.model.OutputFormat
import com.amazonaws.services.polly.model.SynthesizeSpeechRequest
import org.springframework.stereotype.Service

@Service
class AwsPollyService(
        val pollyClient: AmazonPolly
) {

    fun vocalizeText(text: String): ByteArray {
        val voices = pollyClient.describeVoices(DescribeVoicesRequest()).voices
        val speech = pollyClient.synthesizeSpeech(
                SynthesizeSpeechRequest()
                        .withText(text)
                        .withVoiceId(voices[52].id)
                        .withOutputFormat(OutputFormat.Ogg_vorbis)
        )

        return speech.audioStream.readAllBytes()
    }


}