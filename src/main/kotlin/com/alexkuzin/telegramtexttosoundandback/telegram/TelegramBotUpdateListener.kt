package com.alexkuzin.telegramtexttosoundandback.telegram

import com.alexkuzin.telegramtexttosoundandback.service.AwsPollyService
import com.alexkuzin.telegramtexttosoundandback.service.AwsTranscribeService
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.TelegramException
import com.pengrad.telegrambot.UpdatesListener
import com.pengrad.telegrambot.model.Update
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.GetFile
import com.pengrad.telegrambot.request.SendMessage
import com.pengrad.telegrambot.request.SendVoice
import jakarta.annotation.PostConstruct
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class TelegramBotUpdateListener(private val telegramBot: TelegramBot, private val awsTranscribeService: AwsTranscribeService, private val awsPollyService: AwsPollyService, private val restTemplate: RestTemplate) : UpdatesListener {

    @PostConstruct
    fun postConstruct() {
        telegramBot.setUpdatesListener(this
        ) { e ->
            if(e is TelegramException && e.message != null && e.message!!.contains("409")){
                Thread.sleep(5_000)
                postConstruct()
            }else{
                e.printStackTrace()
            }

        }
    }

    override fun process(updates: MutableList<Update>?): Int {
        if (updates == null) return UpdatesListener.CONFIRMED_UPDATES_ALL
        var lastProcessedUpdate = UpdatesListener.CONFIRMED_UPDATES_NONE
        updates.forEach { update ->
            try {
                onUpdate(update)
                lastProcessedUpdate = update.updateId()
            } catch (e: Throwable) {
                System.err.println(e.message)
                System.err.println(e.stackTrace)
                println(e.message)
                println(e.stackTrace)
                return lastProcessedUpdate
            }
        }
        return UpdatesListener.CONFIRMED_UPDATES_ALL
    }

    private fun onUpdate(update: Update) {
        val message = update.message()

        if (message.text() != null && message.text() == "/start") {
            onStartUpdate(update)
        } else if (message.voice() != null) {
            onVoiceUpdate(update)
        } else if (message.text() != null) {
            onTextUpdate(update)
        } else {
            sendTextMessage(update.getChatId(), "This bot can only accept voice and text messages.")
        }
    }

    private fun onStartUpdate(update: Update) {
        sendTextMessage(update.getChatId(),
                    """
                        *Welcome to my voice transcription and text vocalizing bot!*
                        
                        This bot was made using Spring Boot framework and Aws services - S3, Transcribe and Polly!
                        *Only english language currently supported!*
                        
                        [Git repository](https://gitlab.com/AlexKuzin_Hactpnkobn4/telegrambotawstexttosoundandback)
                        [LinkedIn](https://www.linkedin.com/in/alex-kuzin/)
                        
                        Made by [Alex Kuzin](https://t.me/AleksandrKuzin1994)""".trimIndent()
        )
        sendTextMessage(update.getChatId(), "You can start by simply recording voice message or by sending a text message! Processing of voice message can take from about 5 to 15 seconds. Enjoy!")
    }

    private fun onVoiceUpdate(update: Update) {
        val voiceFileURL = update.getVoiceFileUri()
        restTemplate.execute(voiceFileURL, HttpMethod.GET, null, { response ->
            val transcription = awsTranscribeService.transcribe(response.body)
            val message = if (transcription.isNullOrBlank()) {
                "Voice message is too quiet or illegible. Please, try again \uD83D\uDE0A"
            } else transcription
            sendTextMessage(update.getChatId(), message)
        })
    }

    private fun onTextUpdate(update: Update) {
        val data = awsPollyService.vocalizeText(update.message().text())

        telegramBot.execute(SendVoice(update.getChatId(), data))
    }

    private fun sendTextMessage(chatId: Long, message: String) {
        val messageRequest = SendMessage(chatId, message).parseMode(ParseMode.Markdown)

        val response = telegramBot.execute(messageRequest)
        if (!response.isOk) {
            throw RuntimeException("Telegram bot execute message error! Code: ${response.errorCode()}, description: ${response.description()}. ${response.parameters()}")
        }
    }

    private fun Update.getVoiceFileUri(): String {
        val fileId = this.message().voice().fileId()
        val getFileResult = telegramBot.execute(GetFile(fileId))
        return telegramBot.getFullFilePath(getFileResult.file())
    }

    fun Update.getChatId(): Long = this.message().chat().id()
}