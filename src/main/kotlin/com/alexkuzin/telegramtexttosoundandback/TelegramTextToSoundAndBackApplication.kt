package com.alexkuzin.telegramtexttosoundandback

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TelegramTextToSoundAndBackApplication

fun main(args: Array<String>) {
    runApplication<TelegramTextToSoundAndBackApplication>(*args)
}
