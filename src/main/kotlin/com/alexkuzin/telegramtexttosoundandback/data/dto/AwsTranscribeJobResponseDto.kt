package com.alexkuzin.telegramtexttosoundandback.data.dto

import com.google.gson.annotations.SerializedName

data class AwsTranscribeJobResponseDto(
        @SerializedName("jobName") var jobName: String? = null,
        @SerializedName("accountId") var accountId: String? = null,
        @SerializedName("results") var results: Results? = Results(),
        @SerializedName("status") var status: String? = null
)

data class Results(
        @SerializedName("transcripts") var transcripts: ArrayList<Transcripts> = arrayListOf(),
        @SerializedName("items") var items: ArrayList<Items> = arrayListOf()
)

data class Transcripts(
        @SerializedName("transcript") var transcript: String? = null
)

data class Items(
        @SerializedName("start_time") var startTime: String? = null,
        @SerializedName("end_time") var endTime: String? = null,
        @SerializedName("alternatives") var alternatives: ArrayList<Alternatives> = arrayListOf(),
        @SerializedName("type") var type: String? = null
)

data class Alternatives(
        @SerializedName("confidence") var confidence: String? = null,
        @SerializedName("content") var content: String? = null
)