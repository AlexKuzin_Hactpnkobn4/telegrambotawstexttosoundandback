package com.alexkuzin.telegramtexttosoundandback.utils.config

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.polly.AmazonPolly
import com.amazonaws.services.polly.AmazonPollyClientBuilder
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.transcribe.AmazonTranscribe
import com.amazonaws.services.transcribe.AmazonTranscribeClient
import com.google.gson.Gson
import com.pengrad.telegrambot.TelegramBot
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class BeanConfig {

    @Bean
    fun telegramBot(telegramBotConfig: TelegramBotConfig): TelegramBot {
        return TelegramBot(telegramBotConfig.token)
    }

    @Bean
    fun s3Client(awsConfig: AwsConfig): AmazonS3 {
        return AmazonS3Client.builder().withCredentials(
                AWSStaticCredentialsProvider(
                        BasicAWSCredentials(
                                awsConfig.security!!.accessKey,
                                awsConfig.security!!.secretKey
                        )
                )
        ).build()
    }

    @Bean
    fun transcribeClient(awsConfig: AwsConfig): AmazonTranscribe {
        return AmazonTranscribeClient.builder().withCredentials(
                AWSStaticCredentialsProvider(
                        BasicAWSCredentials(
                                awsConfig.security!!.accessKey,
                                awsConfig.security!!.secretKey
                        )
                )
        ).withRegion(Regions.EU_CENTRAL_1).build()
    }

    @Bean
    fun pollyClient(awsConfig: AwsConfig): AmazonPolly {
        return AmazonPollyClientBuilder.standard()
                .withRegion(Regions.EU_CENTRAL_1)
                .build()
    }

    @Bean
    fun restTemplate(): RestTemplate = RestTemplate()

    @Bean
    fun gson(): Gson {
        return Gson()
    }
}