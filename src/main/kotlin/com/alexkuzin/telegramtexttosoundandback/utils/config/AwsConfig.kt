package com.alexkuzin.telegramtexttosoundandback.utils.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("aws")
class AwsConfig(
        var security: Security? = null,
        var transcribe: Transcribe? = null
) {
    data class Security(
            var accessKey: String,
            var secretKey: String
    )

    data class S3(
            var bucketName: String,
            var inputKeyPrefix: String,
            var outputKeyPrefix: String
    )

    data class Transcribe(
            val s3: S3,
            val jobTimeoutMillis: Long,
            val jobStatusRequestingDelayMillis: Long
    )
}