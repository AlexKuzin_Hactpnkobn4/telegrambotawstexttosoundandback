package com.alexkuzin.telegramtexttosoundandback.utils.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("telegram-bot")
data class TelegramBotConfig(
        var token: String = ""
)
