# TelegramBotAwsTextToSoundAndBack

Telegram bot able to transcribe a voice messages and vocalasing text messages.

[Telegram invitation link](https://t.me/AlexKuzinAwsTextBot)

Project was made using Spring boot framework and AWS services: EC2, Transcribe, Polly and S3.

To run this project you must include AWS secret and access keys and Telegram token in your Environment variables.